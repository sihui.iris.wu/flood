# Flood



## Project Description
This project mimics the functionality of the board game Flood. The puzzle game Flood is one of Simon Tatham's collection of GUI games (available on a variety of operating-system configurations, including Ubuntu.) He in turn got the idea from a now-defunct web site: http://floodit.appspot.com. This project is done during the course of CS61B - Data Structures in Fall 2021, taught by Professor Paul Hilfinger in U.C. Berkeley.

